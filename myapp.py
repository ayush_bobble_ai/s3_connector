#https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#S3.Bucket.objects
import boto3
import json
from io import BytesIO
import gzip

s3 = boto3.resource('s3')

bucket = s3.Bucket('bobble-compressed-logs')
for obj in bucket.objects.filter(Prefix='events-gzipped-partitioned/dt=2015-02-17'):
    key = obj.key
    obj = s3.Object('bobble-compressed-logs',key)
    n = obj.get()['Body'].read()
    gzipfile = BytesIO(n)
    gzipfile = gzip.GzipFile(fileobj=gzipfile)
    content = gzipfile.read()
    print(content)